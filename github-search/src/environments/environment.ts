// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  //Link for git hub
  link: 'https://api.github.com/users/',
  //client_id
  clientId:'?client_id=098140ea6fc4e0b2356c',
  //client_secret
  clientSecret: '&client_secret=527b28692f1952c2aa097e8f1e2d9f1c3c537b86'
};
